package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"os"
	"path/filepath"
	"strconv"

	"google.golang.org/grpc"
)

func splitInChunks(caminolibro string, nombrelib string) {
	fileToBeChunked := caminolibro

	file, err := os.Open(fileToBeChunked)

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	defer file.Close()

	fileInfo, _ := file.Stat()

	var fileSize int64 = fileInfo.Size()

	const fileChunk = 256000 //0.256 * (1 << 20) 1 MB, change this to your requirement

	// calculate total number of parts the file will be chunked into

	totalPartsNum := uint64(math.Ceil(float64(fileSize) / float64(fileChunk)))

	fmt.Printf("Splitting to %d pieces.\n", totalPartsNum)

	for i := uint64(0); i < totalPartsNum; i++ {

		partSize := int(math.Min(fileChunk, float64(fileSize-int64(i*fileChunk))))
		partBuffer := make([]byte, partSize)

		file.Read(partBuffer)

		// write to disk
		fileName := nombrelib + "_" + strconv.FormatUint(i, 10)
		_, err := os.Create(fileName)

		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// write/save buffer to disk
		ioutil.WriteFile(fileName, partBuffer, os.ModeAppend)

		fmt.Println("Split to : ", fileName)
	}
}

func main() {
	var i int
	var listalibros []string
	root := "./Libros"

	fmt.Println("¿Que desea hacer? : \n1.-Cargar libros\n2.-Descargar libros")
	fmt.Println("Digite el número de la opción: ")
	fmt.Scan(&i)

	if i == 1 {
		conn, errconn := grpc.Dial(":9002", grpc.WithInsecure()) //Conexion datanode

		if errconn != nil {
			log.Fatalf("Se casho el sistema: %s", errconn)
		}

		defer conn.Close()

		fmt.Println("¿Que libro quieres cargar? :")
		err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
			listalibros = append(listalibros, info.Name())
			return nil
		})
		if err != nil {
			panic(err)
		}

		listalibros = listalibros[1:]

		var i int = 1
		var n int

		for _, file := range listalibros {

			fmt.Printf("%d.-%s\n", i, file)
			i = i + 1
		}
		fmt.Println("")

		fmt.Println("Digite el número de la opción: ")

		fmt.Scan(&n)

		libroSel := "./Libros/" + listalibros[n-1]

		//Romper libro >:c
		splitInChunks(libroSel, listalibros[n-1])

	} else {
		//conn, err := grpc.Dial(":9001", grpc.WithInsecure())  Conexion NameNode

		fmt.Println("descargar")
	}
}
