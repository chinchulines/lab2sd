package carga

import (
	"log"

	"golang.org/x/net/context"
)

type Server struct {
}

func (s *Server) UploadService(ctx context.Context, message *Data) (*Data, error) {
	log.Printf("Mensaje recibido del cliente: %s", message.Info)
	return &Data{Info: "Hola desde el server!"}, nil
}
