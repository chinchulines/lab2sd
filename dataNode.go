package main

import (
	"log"
	"net"

	"gitlab.com/chinchulines/lab2sd/carga"
	"google.golang.org/grpc"
)

func main() {
	lis, err := net.Listen("tcp", ":9002") //conexion cliente -- SERVER

	if err != nil {
		log.Fatalf("Se casho el sistema: %s", err)
	}

	defer lis.Close()

	s := carga.Server{}

	grpcServer := grpc.NewServer()

	carga.RegisterUploadServiceServer(grpcServer, &s)

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("Falla del server en el puerto 9002: %v", err)
	}
}
